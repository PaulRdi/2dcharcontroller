﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ComponentBased2DController
{
    [RequireComponent(typeof(CharacterController))]
    class GravityEffector : Effector
    {

        [SerializeField] AnimationCurve gravityCurve;

        public float Gravity
        {
            get
            {
                return gravity;
            }
        }
        [SerializeField] float gravity = 9.81f;
        [SerializeField] float timeForMaxGravity = 0.7f;
        float timeFallen;

        public override Vector2 ProcessVelocity(State state, Vector2 velocity)
        {
            if (state is AirbornState)
            {
                velocity.y -= gravity * gravityCurve.Evaluate(Mathf.Clamp(timeFallen / timeForMaxGravity, 0f, 1f));
                timeFallen += Time.deltaTime;
                return velocity;
            }
            else
            {
                timeFallen = 0f;
            }
            return velocity;
        }
    }
}