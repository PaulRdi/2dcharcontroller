﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ComponentBased2DController
{
    public class AerialControlAbility : Ability
    {
        [SerializeField] float horizontalInfluence = 1.0f;

        public override void Init()
        {
        }

        public override Vector2 ProcessVelocity(State currentState, Vector2 velocity)
        {
            if (currentState is AirbornState)
            {
                float hozInput = Input.GetAxis("Horizontal");
                velocity.x += hozInput * horizontalInfluence * Time.deltaTime;
            }
            return velocity;
        }
    }
}
