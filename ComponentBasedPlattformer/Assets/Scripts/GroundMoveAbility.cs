﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ComponentBased2DController
{
    public class GroundMoveAbility : Ability
    {

        [SerializeField] float minMoveSpeed, maxMoveSpeed;
        [SerializeField] AnimationCurve accelerationCurve, breakCurve;
        [SerializeField] float accelerationTime, breakTime;

        float hozInput;
        float currHozSpeed;
        float timePassedAccelerating, timePassedBreaking;

        public override void Init()
        {
            timePassedAccelerating = timePassedBreaking = 0f;
        }

        public override Vector2 ProcessVelocity(State currentState, Vector2 velocity)
        {
            if (currentState is GroundedState)
            {
                hozInput = Input.GetAxis("Horizontal");

                if (Mathf.Abs(hozInput) <= .001f)
                {
                    if (breakTime == 0f)
                    {
                        currHozSpeed = 0f;
                    }
                    else
                    {
                        float t = Mathf.Clamp(timePassedBreaking / breakTime, 0f, 1f);
                        currHozSpeed = Mathf.Lerp(0, currHozSpeed, breakCurve.Evaluate(t));
                        timePassedBreaking += Time.deltaTime;
                    }
                    timePassedAccelerating = 0f;

                }
                else
                {
                    if (accelerationTime == 0f)
                    {
                        currHozSpeed = maxMoveSpeed * Mathf.Sign(hozInput);
                    }
                    else
                    {
                        float t = Mathf.Clamp(timePassedAccelerating / accelerationTime, 0f, 1f);
                        currHozSpeed = Mathf.Lerp(minMoveSpeed, maxMoveSpeed, accelerationCurve.Evaluate(t)) * Mathf.Sign(hozInput);
                        timePassedAccelerating += Time.deltaTime;
                    }
                    timePassedBreaking = 0f;

                }

                velocity.x = currHozSpeed ;
            }


            return velocity;
        }
    }
}