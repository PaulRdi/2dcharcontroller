﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ComponentBased2DController
{
    public class JumpAbility : Ability
    {

        [SerializeField] float jumpStrength = 50f;
        [SerializeField] float timeBeforeNextJump = 0.5f;

        float timer;
        bool timerReady;
        public override void Init()
        {
            timer = 0f;
            timerReady = true;
        }

        public override Vector2 ProcessVelocity(State currentState, Vector2 velocity)
        {
            if(timerReady && currentState is GroundedState)
            {
                if (Input.GetButton("Jump"))
                {
                    velocity.y += jumpStrength;
                    timerReady = false;
                    timer = 0f;
                }
            }

            if (!timerReady && timer < timeBeforeNextJump)
            {
                timer += Time.deltaTime;
            }
            else
            {
                timer = 0f;
                timerReady = true;
            }

            return velocity;
        }

        
    }
}
