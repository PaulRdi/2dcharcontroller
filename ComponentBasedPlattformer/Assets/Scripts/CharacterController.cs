﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using System.Reflection.Emit;
using System.Linq;

namespace ComponentBased2DController
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class CharacterController : MonoBehaviour
    {

        public event Action<State, CharacterController> stateChanged;
        public event Action<State, CharacterController> stateUpdated;

        //Static hashtable which maps from Character Name to CharacterConstraints
        static Hashtable characters;

        [SerializeField] GameObject footObject;

        Effector[] characterEffectors;
        Ability[] characterAbilities;
        Hashtable characterStates;
        public CharacterData myData;

        [Header("Constraints")]
        [SerializeField] float maxVerticalSpeed;
        [SerializeField] float maxHorizontalSpeed;
        

        public State State
        {
            get
            {
                return state;
            }
        }
        State state;

        private void Awake()
        {
            if (characters == null)
            {
                characters = new Hashtable();
            }

            characters.Add(gameObject.name, this);


            myData = new CharacterData(GetComponentsInChildren<Collider2D>(), footObject.GetComponent<Collider2D>(), GetComponent<Rigidbody2D>());

            characterEffectors = GetComponents<Effector>();
            characterEffectors = characterEffectors.OrderByDescending(e => e.priority).ToArray();

            characterAbilities = GetComponents<Ability>();
            foreach (Ability a in characterAbilities)
            {
                a.Init();
            }
            characterAbilities = characterAbilities.OrderByDescending(ability => ability.priority).ToArray();

            characterStates = new Hashtable();

            foreach (var c in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (c.BaseType == typeof(State))
                {
                    State s = (State)Activator.CreateInstance(c);
                    characterStates.Add(s.Name, s);
                }                             
            }

            TransitionToState("AirbornState", this.gameObject);
        }

        public static void TransitionToState(string stateName, GameObject sender)
        {
            CharacterController controller = (CharacterController)characters[sender.name];
            if (controller.state != null)
                controller.state.OnExit(controller.myData);

            controller.state = (State)controller.characterStates[stateName];
            controller.state.OnEnter(controller.myData);

            controller.stateChanged?.Invoke(controller.state, controller);
        }

        void Update() {
            int colliderCount = myData.footCollider.OverlapCollider(new ContactFilter2D(), myData.collidersIntersectingFoot);
            myData.collidersIntersectingFootLength = colliderCount;

            state.OnUpdate(myData);
            stateUpdated?.Invoke(state, this);

            foreach (Effector e in characterEffectors)
            {
                if (!e.enabled) continue;
                myData.rigidbody.velocity = e.ProcessVelocity(state, myData.rigidbody.velocity);
            }
            foreach (Ability a in characterAbilities)
            {
                if (!a.enabled) continue;
                myData.rigidbody.velocity = a.ProcessVelocity(state, myData.rigidbody.velocity);
            }

            myData.rigidbody.velocity = new Vector2(Mathf.Clamp(myData.rigidbody.velocity.x, -maxHorizontalSpeed, maxHorizontalSpeed),
                                                    Mathf.Clamp(myData.rigidbody.velocity.y, -maxVerticalSpeed, maxVerticalSpeed));
        }

        
    }


    public abstract class Effector : MonoBehaviour
    {
        public bool enabled = true;
        public int priority = 1;
        public abstract Vector2 ProcessVelocity(State currentState, Vector2 velocity);
    }

    public abstract class Ability : MonoBehaviour
    {
        public bool enabled = true;
        public int priority = 1;
        public abstract Vector2 ProcessVelocity(State currentState, Vector2 velocity);
        public abstract void Init();
    }

    public class State
    {

        public string Name
        {
            get
            {
                return name;
            }
        }
        string name;

        public State()
        {
            //string split, weil GetType den Namespace mit drinnen hat.
            //geht bestimmt eleganter.
            name = GetType().ToString().Split('.')[1];
        }

        public virtual void OnEnter(CharacterData data)
        {

        }
        public virtual void OnExit(CharacterData data)
        {

        }
        public virtual void OnUpdate(CharacterData data)
        {

        }
    }

    public class CharacterData
    {
        public Collider2D[] colliders;
        public Collider2D[] collidersIntersectingFoot;
        public int collidersIntersectingFootLength;
        public Rigidbody2D rigidbody;
        public Collider2D footCollider;

        public CharacterData (Collider2D[] colliders, Collider2D footCollider, Rigidbody2D rigidbody)
        {
            this.colliders = colliders;
            this.rigidbody = rigidbody;
            this.footCollider = footCollider;
            collidersIntersectingFoot = new Collider2D[64];
        }
    }
}