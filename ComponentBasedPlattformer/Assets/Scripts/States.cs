﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ComponentBased2DController
{
    class GroundedState : State
    {
        public GroundedState() : base()
        {

        }

        public override void OnEnter(CharacterData data)
        {
            base.OnEnter(data);
            data.rigidbody.velocity = new Vector2(data.rigidbody.velocity.x, 0f);
        }

        public override void OnExit(CharacterData data)
        {
            base.OnExit(data);
        }

        public override void OnUpdate(CharacterData data)
        {
            base.OnUpdate(data);
            bool stillGrounded = false;
            foreach (Collider2D col in data.collidersIntersectingFoot)
            {
                if (col == null) continue;
                if (col.gameObject.tag == "Ground")
                {
                    stillGrounded = true;
                    break;
                }
            }

            if (!stillGrounded)
            {
                CharacterController.TransitionToState("AirbornState", data.rigidbody.gameObject);
            }
        }
    }


    class AirbornState : State
    {
        public AirbornState() : base()
        {
        }

        public override void OnEnter(CharacterData data)
        {
            base.OnEnter(data);
        }

        public override void OnExit(CharacterData data)
        {
            base.OnExit(data);
        }

        public override void OnUpdate(CharacterData data)
        {
            base.OnUpdate(data);            
            foreach(Collider2D col in data.collidersIntersectingFoot)
            {
                if (col == null) continue;
                if (col.gameObject.tag == "Ground")
                {
                    CharacterController.TransitionToState("GroundedState", data.rigidbody.gameObject);
                    break;
                }
            }
        }
    }
}