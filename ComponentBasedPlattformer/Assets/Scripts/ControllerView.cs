﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ComponentBased2DController
{
    [RequireComponent(typeof(CharacterController))]
    public class ControllerView : MonoBehaviour
    {
        [SerializeField] Transform spriteTransform;
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] Animator spriteAnimator;

        CharacterController characterController;
        Vector3 ogScale;

        private void Awake()
        {
            characterController = GetComponent<CharacterController>();
            characterController.stateUpdated += CharacterController_stateUpdated;
            characterController.stateChanged += CharacterController_stateChanged;
            ogScale = gameObject.transform.localScale;
        }

        private void CharacterController_stateChanged(State state, CharacterController controller)
        {
            if (state is GroundedState)
            {
                spriteAnimator.SetTrigger("Land");
            }
        }

        private void CharacterController_stateUpdated(State obj, CharacterController controller)
        {
            if (Mathf.Abs(controller.myData.rigidbody.velocity.x) > 0f)
                transform.localScale = new Vector3(ogScale.x * Mathf.Sign(controller.myData.rigidbody.velocity.x), ogScale.y, ogScale.z);
        }
    }
}
